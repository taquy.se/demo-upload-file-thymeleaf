package app.helper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import app.dto.UploadFormDto;

public class Uploader {
	public static Model upload(HttpServletRequest request, Model model, UploadFormDto form) {
		 
        String description = form.getDescription();
        String dir = request.getServletContext().getRealPath("upload");
 
        File root = new File(dir);
        if (!root.exists()) root.mkdirs();
        
        
        MultipartFile[] datas = form.getFileDatas();

        List<File> files = new ArrayList<File>();
        List<String> fails = new ArrayList<String>();
 
        for (MultipartFile data : datas) {
 
            String name = data.getOriginalFilename();
 
            if (name == null || name.length() <= 0) continue;
            
            try {
            	
                File file = new File(root.getAbsolutePath() + File.separator + name);
                FileOutputStream fos = new FileOutputStream(file);
 
                BufferedOutputStream stream = new BufferedOutputStream(fos);
                stream.write(data.getBytes());
                stream.close();
                    
                files.add(file);
            } catch (Exception e) {
            	fails.add(name);
            }
        }
        
        model.addAttribute("description", description);
        model.addAttribute("uploadedFiles", files);
        model.addAttribute("failedFiles", fails);
        
        return model;
    }
}
