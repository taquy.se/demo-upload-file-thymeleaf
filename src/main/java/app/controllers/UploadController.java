package app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import app.dto.UploadFormDto;
import app.helper.Uploader;

@Controller
public class UploadController {
 
    @RequestMapping(value = "/")
    public String home() {
        return "index";
    }
 
    @GetMapping(value = "/upload/one")
    public String one(Model model) {
 
        UploadFormDto form = new UploadFormDto();
        model.addAttribute("form", form);
 
        return "upload/one";
    }
    
    @GetMapping(value = "/upload/multi")
    public String multi(Model model) {
 
    	UploadFormDto dto = new UploadFormDto();
        model.addAttribute("form", dto);
 
        return "upload/multi";
    }
    
    @PostMapping(value = "/upload/one")
    public String upOne(HttpServletRequest request, Model model, @ModelAttribute("form") UploadFormDto form) {
    	
    	Uploader.upload(request, model, form);
        return "upload/result";
 
    }
 
    @PostMapping(value = "/upload/multi")
    public String upMulti(HttpServletRequest request, Model model, @ModelAttribute("form") UploadFormDto form) {
    	
    	Uploader.upload(request, model, form);
    	return "upload/result";
    	
    }
 
}